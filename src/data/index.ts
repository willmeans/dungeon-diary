export type Info = {
    name: string,
    race: string,
    class: string,
    level: number,
    exp: number,
    alignment: string,
    party: string,
}

export type CoreStats = {
    str: number,
    str_modifier: number | undefined,
    dex: number,
    dex_modifier: number | undefined,
    con: number,
    con_modifier: number | undefined,
    int: number,
    int_modifier: number | undefined,
    wis: number,
    wis_modifier: number | undefined,
    cha: number
    cha_modifier: number | undefined
}

export type OtherStats = {
    ac: number,
    hp_max: number,
    hp_tmp: number,
    hp_current: number,
    proficiencyBonus: number,
    passivePerception: number,
    initiative: number,
    speed: number,
    vision: number
}

export type ProficienciesAndLanguages = string[]

export type Inventory = string[]

export type FeaturesAndTraits = string[]
export type AttacksAndSpells = string[]

export type Dice = {
    quantity: number,
    sides: number
}
//! Also works for cantrips
export type Spell = {
    name: string,
    dice?: Dice,
    magnitude: number,
    description: string
}
export type Spells = Spell[]

export type Character = {
    isValid: boolean,
    summary: Info,
    coreStats: CoreStats,
    otherStats: OtherStats,
    proficiencies: ProficienciesAndLanguages,
    inventory: Inventory,
    weapons: Inventory,
    features: FeaturesAndTraits,
    attacks: AttacksAndSpells,
    spells: AttacksAndSpells,
    sprells: Spells,
    cantrips: AttacksAndSpells
}