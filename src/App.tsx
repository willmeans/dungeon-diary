import React, { useState, useEffect, useReducer } from 'react';
import './App.css';

//! Material UI
import { ThemeProvider, createTheme } from '@mui/material/styles'
import AppBar from '@mui/material/AppBar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import SaveIcon from '@mui/icons-material/Save';
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'

import { JsonViewer } from '@textea/json-viewer'

//! react-router
import { Routes, Route, Link } from 'react-router-dom'

import Menu from './components/navigation'
import Main from './components/pages/character/main'
import Summary from './components/character/summary'
import Stats from './components/character/stats'
import ListStats from './components/character/listStats'
import services from './services'

import type { Character, Info } from './data'
import CharacterReducer from './services/CharacterReducer';

const customTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const characterProvider = services.CharacterProvider()

const startingCharacter = characterProvider.get()

type TestComponentProps = {
    character: Character
}

function Test(character: TestComponentProps) {
  return (
    <>
      <div style={{ textAlign: "left" }}>
        <JsonViewer value={character} onChange={(x)=>console.log(x)} />
      </div>
    </>
  )
}

function App() {

  const [state, dispatch] = useReducer(CharacterReducer, startingCharacter)

  const saveCharacter = () => {
    console.log('saving')
    characterProvider.set({ ...state, isValid: true })
    characterProvider.save()
  }

  //TODO: doesn't work, needs manual save
  useEffect(() => {
    return () => {
      saveCharacter()
    }
  }, [])

  return (
    <div className="App">
      <ThemeProvider theme={customTheme}>
        <CssBaseline />
        <AppBar sx={{ ml: '1em', textAlign: 'left'}}>
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              Character Journal
            </Typography>
            <Button onClick={saveCharacter} startIcon={<SaveIcon />} />
          </Toolbar>
        </AppBar>
        <Toolbar />
        <Menu />


        <Routes>
          <Route path='/' element={<Main character={state} dispatch={dispatch} />} />
          <Route path='/summary' element={<Summary character={state} dispatch={dispatch} />} />
          <Route path='/stats' element={<Stats character={state} dispatch={dispatch} />} />
          <Route path='/abilities' element={<ListStats character={state} element="abilities" dispatch={dispatch} />} />
          <Route path='/spells' element={<ListStats character={state} element="spells" dispatch={dispatch} />} />
          <Route path='/inventory' element={<ListStats character={state} element="inventory" dispatch={dispatch} />} />
          <Route path='/features' element={<ListStats character={state} element="features" dispatch={dispatch} />} />
          <Route path='/attacks' element={<ListStats character={state} element="attacks" dispatch={dispatch} />} />
          <Route path='/test' element={<Test character={state} />} />
        </Routes>
      </ThemeProvider>
    </div>
  );
}

export default App;
