import React, { ChangeEvent } from 'react';

//! Material UI
import { ThemeProvider, createTheme } from '@mui/material/styles'
import Typography from '@mui/material/Typography'

import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';

import Summary from '../../../character/summary'
import Stats from '../../../character/stats'
import ListStats from '../../../character/listStats'
import StatList from '../../../character/statList'
// import services from './services'

import type { Character, Info } from '../../../../data'
import { CharacterActions } from '../../../../services/CharacterReducer';
import SpellView from '../../../utils/SpellView';

const customTheme = createTheme({
    palette: {
        // mode: 'dark',
    },
});

type ComponentProps = {
    character: Character,
    dispatch: (action: CharacterActions) => void,
}

function Main(props: ComponentProps): React.ReactElement {
    const { character, dispatch } = props
    const { summary } = character

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newSummary = { ...summary, [event.target.id]: event.target.value }
    }

    //! Handles "list element" change
    const handleLeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.target
        const splitValue = value.split('\n')
        // dispatch(id, splitValue)
    }

    return (
        <Container sx={{ padding: 0 }}>
            <Box
                sx={{
                    '& .MuiTextField-root': { m: 1, width: '8ch', size: 18 },
                    '& .MuiForm//label-root': { marginBottom: 1, fontSize: '1.7rem', overflow: 'unset', position: 'relative' },
                    '& .MuiInputBase-root': { fontSize: '1.5rem' },
                }}
            >
                <Summary character={character} dispatch={dispatch} />
                {/* <SpellView character={character} sprellType="cantrips" dispatch={dispatch} /> */}
                <Stats character={character} dispatch={dispatch} />
                <Paper>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <StatList editable elements={character.proficiencies} title="Proficiencies" onSave={(elements) => dispatch(

                                { type: 'UPDATE_PROFICIENCIES', data: elements }

                            )} />
                        </Grid>
                        {/* <Grid item md={6} xs={12}>
                            <ListStats character={character} element="proficiencies" dispatch={dispatch} />
                        </Grid> */}
                        <Grid item md={6} xs={12}>
                            <StatList editable elements={character.inventory} title="Inventory" onSave={(elements) => dispatch(
                                { type: 'UPDATE_INVENTORY', data: elements }
                            )} />
                            {/* <ListStats character={character} element="inventory" dispatch={dispatch} /> */}
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <ListStats character={character} element="weapons" dispatch={dispatch} />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <ListStats character={character} element="features" dispatch={dispatch} />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <ListStats character={character} element="attacks" dispatch={dispatch} />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <ListStats character={character} element="spells" dispatch={dispatch} />
                        </Grid>
                        <Grid item md={6} xs={12}>
                            <ListStats character={character} element="cantrips" dispatch={dispatch} />
                        </Grid>
                    </Grid>
                </Paper>
            </Box>
        </Container>
    );
}

export default Main;
