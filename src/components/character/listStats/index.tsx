import React, { useState, useEffect, ChangeEvent } from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import type { Character } from '../../../data'
import { CharacterActions } from '../../../services/CharacterReducer';

type ComponentProps = {
  character: Character,
  element: string,
  dispatch: (action: CharacterActions) => void
}

function ListStats(props: ComponentProps): React.ReactElement {
  const { character, element, dispatch } = props
  const { summary } = character
  const defaultValue = (character as any)[element]

  const [content, setContent] = useState(defaultValue.join('\n'))

  //! Handles list element change
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = event.target
    const splitValue = value.split('\n')
    // setListElement(element || id, splitValue)
    setContent(value)
  }

  const saveChanges = () => {
    const splitValue = content.split('\n')
    // TODO: setting the cantrip update action to the default has a
    //       bug where we can accidentally update cantrips if element
    //       is invalud
    let action: CharacterActions = {type: 'UPDATE_CANTRIPS', data: splitValue}
    if (element === 'inventory')
      action = { type: 'UPDATE_INVENTORY', data: splitValue }
    else if (element === 'weapons')
      action = { type: 'UPDATE_WEAPONS', data: splitValue }
    else if (element === 'features')
      action = { type: 'UPDATE_FEATURES_AND_TRAITS', data: splitValue }
    else if (element === 'attacks')
      action = { type: 'UPDATE_ATTACKS', data: splitValue }
    else if (element === 'spells')
      action = { type: 'UPDATE_SPELLS', data: splitValue }

    dispatch(action)
  }

  //TODO: see if sx prop here is what was causing issues with NumericTextField
  return (
    <Container>
      <Typography variant="h6">{element}</Typography>
      <TextField
        multiline
        sx={{ minWidth: "100%" }}
        rows={5}
        id={element}
        // label={element}
        variant="outlined"
        onChange={handleChange}
        onBlur={saveChanges}
        defaultValue={content}
      />
    </Container>
  )
}

export default ListStats