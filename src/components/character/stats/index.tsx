import React, { ChangeEvent } from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

import { modifierCalculator } from '../../../services/character/stats'

import type { Character, Info } from '../../../data'

import { StatModifierView } from './StatModifierView'
import CoreStatsView from './core'
import OtherStatsView from './other'
import { CharacterActions } from '../../../services/CharacterReducer';


type ComponentProps = {
  character: Character,
  dispatch: (action: CharacterActions) => void
}

function Stats(props: ComponentProps): React.ReactElement {
  const { character, dispatch } = props

  return (
    <Container>
      <Box
        sx={{
          '& .MuiTextField-root': { m: 1, width: '8ch', size: 18 },
          '& .MuiForm//label-root': { marginBottom: 1, fontSize: '1.7rem', overflow: 'unset', position: 'relative' },
          '& .MuiInputBase-root': { fontSize: '1.5rem' },
        }}
      >
        <Grid container>
          <Grid item md={3}>
            <CoreStatsView character={character} dispatch={dispatch} />
          </Grid>
          <Grid item md={9}>
            <OtherStatsView character={character} dispatch={dispatch} />
          </Grid>
        </Grid>
      </Box>
    </Container>
  )
}

export default Stats