
import React, { ChangeEvent } from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

import { modifierCalculator } from '../../../../services/character/stats'

import type { Character, Info } from '../../../../data'

import { StatModifierView } from '../StatModifierView'
import { CharacterActions } from '../../../../services/CharacterReducer';


type ComponentProps = {
    character: Character,
    dispatch: (action: CharacterActions) => void
}

function CoreStatsView(props: ComponentProps): React.ReactElement {
    const { character, dispatch } = props
    const { coreStats } = character

    const handleCoreStatChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.target
        const modifier = modifierCalculator(parseInt(value))
        const mod_name = [id] + '_modifier'
        const newCoreStats = { ...coreStats, [id]: value, [mod_name]: modifier }
        dispatch({ type: 'UPDATE_CORE_STATS', data: newCoreStats})
    }

    return (
        <Container>
            <Paper>
                <Grid sx={{ m: 1 }} container>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={"athletics"}>
                        <Typography variant="h6">Strength</Typography>
                        <TextField
                            id="str"
                            //label="Strength"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.str}
                        />
                        <StatModifierView modifier={coreStats.str_modifier} />
                        </span>
                    </Grid>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={"acrobatics\nsleight of hand\nstealth"}>
                        <Typography variant="h6">Dexterity</Typography>
                        <TextField
                            id="dex"
                            //label="Dexterity"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.dex}
                        />
                        <StatModifierView modifier={coreStats.dex_modifier} />
                        </span>
                    </Grid>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={""}>
                        <Typography variant="h6">Constitution</Typography>
                        <TextField
                            id="con"
                            //label="Constitution"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.con}
                        />
                        <StatModifierView modifier={coreStats.con_modifier} />
                        </span>
                    </Grid>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={"arcana\nhistory\ninvestigation\nnature\nreligion"}>
                        <Typography variant="h6">Intelligence</Typography>
                        <TextField
                            id="int"
                            //label="Intelligence"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.int}
                        />
                        <StatModifierView modifier={coreStats.int_modifier} />
                        </span>
                    </Grid>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={"animal handling\ninsight\nmedicine\nperception\nsurvival"}>
                        <Typography variant="h6">Wisdom</Typography>
                        <TextField
                            id="wis"
                            //label="Wisdom"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.wis}
                        />
                        <StatModifierView modifier={coreStats.wis_modifier} />
                        </span>
                    </Grid>
                    <Grid item xs={6} md={12} className="stat-display-item">
                        <span title={"deception\nintimidation\nperformance\npersuasion"}>
                        <Typography variant="h6">Charisma</Typography>
                        <TextField
                            id="cha"
                            //label="Charisma"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleCoreStatChange}
                            defaultValue={coreStats.cha}
                        />
                        <StatModifierView modifier={coreStats.cha_modifier} />
                        </span>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    )
}

export default CoreStatsView