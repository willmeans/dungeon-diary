import React from 'react'

import Typography from '@mui/material/Typography';

type ComponentProps = {
    modifier: number | undefined
}

/*! A component for displaying status modifiers consistently
 */
export function StatModifierView(props: ComponentProps): React.ReactElement {
    const { modifier } = props
    return (
        <>
            {modifier}
        </>
    )
}

type StatViewProps = {
    stat: number,
    modifier?: number | undefined,
    id: string,
    label: string,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

/*! A component for displaying status modifiers consistently
 */
export function StatView(props: StatViewProps): React.ReactElement {
    const { modifier, id, onChange, stat, label } = props
    return (
        <>
            <Typography variant="h5">
                <label htmlFor={id}>{label}</label>
            </Typography>
            <input id={id} onChange={onChange} type="numeric" value={stat} />
            {modifier}
        </>
    )
}

export default { StatModifierView, StatView }