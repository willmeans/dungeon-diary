import React, { ChangeEvent } from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

import { modifierCalculator } from '../../../../services/character/stats'

import type { Character, Info } from '../../../../data'

import { StatModifierView } from '../StatModifierView'
import { CharacterActions } from '../../../../services/CharacterReducer';


type ComponentProps = {
    character: Character,
    dispatch: (action: CharacterActions) => void
}

function OtherStatsView(props: ComponentProps) {
    const { character, dispatch } = props
    const { otherStats } = character
    const handleExtStatChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = event.target
        const newOtherStats = { ...otherStats, [id]: value }
        dispatch({ type: 'UPDATE_OTHER_STATS', data: newOtherStats})
    }

    return (
        <>
            <Paper>
                <Grid sx={{ m: 1 }} container>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Armor Class</Typography>
                        <TextField
                            id="ac"
                            //label="Armor Class"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.ac}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Hit Points (max)</Typography>
                        <TextField
                            id="hp_max"
                            //label="Hit Points"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.hp_max}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Hit Points (current)</Typography>
                        <TextField
                            id="hp_current"
                            //label="Hit Points (Current)"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.hp_current}
                        />
                    </Grid>
                </Grid>
                <Grid sx={{ m: 1 }} container>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Proficiency Bonus</Typography>
                        <TextField
                            id="proficiencyBonus"
                            //label="Proficiency Bonus"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.proficiencyBonus}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Passive Perception</Typography>
                        <TextField
                            id="passivePerception"
                            //label="Passive Perception"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.passivePerception}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Initiative</Typography>
                        <TextField
                            id="initiative"
                            //label="Initiative"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.initiative}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Movement Speed</Typography>
                        <TextField
                            id="speed"
                            //label="Movement Speed"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.speed}
                        />
                    </Grid>
                    <Grid item xs={4} className="stat-display-item">
                        <Typography variant="h6">Vision</Typography>
                        <TextField
                            id="vision"
                            //label="Vision"
                            variant="outlined"
                            inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            onChange={handleExtStatChange}
                            defaultValue={otherStats.vision}
                        />
                    </Grid>
                </Grid>
            </Paper>
        </>
    )
}

export default OtherStatsView