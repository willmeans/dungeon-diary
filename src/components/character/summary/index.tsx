import React, { useState, ChangeEvent } from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';

import type { Character, Info } from '../../../data'
import { CharacterActions } from '../../../services/CharacterReducer';

type ComponentProps = {
  character: Character,
  dispatch: (action: CharacterActions) => void
}

function Summary(props: ComponentProps): React.ReactElement {
  const { character, dispatch } = props
  const { summary } = character

  const [workingSummary, setWorkingSummary] = useState(summary)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSummary = { ...summary, [event.target.id]: event.target.value }
    setWorkingSummary(newSummary)
  }

  const saveChanges = () => {
    dispatch({type: 'UPDATE_SUMMARY', data: workingSummary})
  }

  const minWidth = "70%"

  //TODO: see if sx prop here is what was causing issues with NumericTextField
  return (
    <Container>
      <Box
        sx={{
          '& .MuiTextField-root': { m: 1 },
        }}
      >
        <Paper
          sx={{
            paddingTop: 4,
            paddingBottom: 4,
          }}
        >

          <Grid container>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="name" label="Name" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.name} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="race" label="Race" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.race} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="class" label="Class" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.class} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="alignment" label="Alignment" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.alignment} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="level" label="Level" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.level} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="exp" label="Experience" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.exp} />
            </Grid>
            <Grid sm={12} md={6}>
              <TextField sx={{ minWidth }} id="party" label="Party" variant="outlined" onBlur={saveChanges} onChange={handleChange} defaultValue={summary.party} />
            </Grid>
          </Grid>
        </Paper>
      </Box>
    </Container >
  )
}

export default Summary