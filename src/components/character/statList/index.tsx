import React, { useState, useEffect, ChangeEvent } from 'react';
import Box from '@mui/material/Box';
import Icon from '@mui/material/Icon';
import Container from '@mui/material/Container';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Paper from '@mui/material/Paper';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';

import type { Character } from '../../../data'
import { CharacterActions } from '../../../services/CharacterReducer';




type ComponentProps = {
  elements: string[],
  title?: string,
  editable: boolean,
  onSave?: (elements: string[]) => void
}

function StatList(props: ComponentProps): React.ReactElement {
  const { elements, title, editable, onSave } = props

  const [content, setContent] = useState(elements)

  const [newElement, setNewElement] = useState('')

  //! Handles list element change
  const addElement = () => { //event: React.ChangeEvent<HTMLInputElement>) => {

    if (!newElement) {
      return
    }
    const newElements = [newElement, ...content]
    setContent(newElements)
    if (onSave) {
      console.log(newElements)
      onSave(newElements)
    }
    setNewElement('')
  }
  const removeElement = (idx: number) => {
    let newElements: string[] = [']']
    if (idx < 0) {

    } if (idx === 0) {
      newElements = [...content.slice(1)]

    } else if (idx < (content.length - 1)) {
      newElements = [...content.slice(0, idx), ...content.slice(idx + 1, content.length)]
    } else {
      newElements = [...content.slice(0, content.length - 1)]
    }
    if (newElements?.length) {
      setContent(newElements)
      if (onSave) {
        onSave(newElements)
      }
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewElement(event.target.value)
  }

  //TODO: see if sx prop here is what was causing issues with NumericTextField
  return (
    <Container>
      <Typography variant="h6">
        {title}
      </Typography>
      {editable &&
        <div style={{padding: "1rem"}}>
          <TextField onChange={handleChange} value={newElement} style={{minWidth: "90%"}}>
          </TextField>
          <AddCircleOutlineOutlinedIcon onClick={addElement} />
        </div>
      }
      <List style={{paddingLeft:"0", paddingRight:"0"}}>
        {content.map((element, idx) => {
          return (<>
            <ListItem>
              <Typography style={{ display: "block", minWidth: "90%" }}>
                {element}
              </Typography>
              {editable &&
                <DeleteOutlineOutlinedIcon onClick={() => { removeElement(idx) }} />
              }
            </ListItem>
          </>
          )
        })}
      </List>

    </Container>
  )
}

export default StatList