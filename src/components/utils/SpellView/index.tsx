import React, { useState, ChangeEvent } from 'react';

//! Material UI
import { ThemeProvider, createTheme } from '@mui/material/styles'
import Typography from '@mui/material/Typography'

import TextField from '@mui/material/TextField';

import type { Character, Spell } from '../../../data'
import { CharacterActions } from '../../../services/CharacterReducer';


type SingleSprellComponentProps = {
  sprell: Spell,
  editable: boolean,
  addElementCallback?: () => void

  // dispatch: (action: CharacterActions) => void
}
function SingleSprellView(props: SingleSprellComponentProps): React.ReactElement {
  const { sprell, editable, addElementCallback } = props

  const diceDescription: string = sprell.dice?.quantity + 'd' + sprell.dice?.sides
  console.log(sprell)

  if (editable)
    return (
      <div className="sprell-view" style={{ display: "flex" }}>
        <input className="sprellement" value={sprell.name} />
        <input className="sprellement" value={sprell.dice && diceDescription} />
        <input className="sprellement" value={sprell.magnitude} />
        <input className="sprellement" value={sprell.description} />
        {
          editable &&
          <div className="addsprell" onClick={addElementCallback}>
            +
          </div>
        }
      </div>
    )
  else
    return (
      <div className="sprell-view" style={{ display: "flex" }}>
        <div className="sprellement">
          {sprell.name}
        </div>
        <div className="sprellement">
          {sprell.dice && diceDescription}
        </div>
        <div className="sprellement">
          {sprell.magnitude}
        </div>
        <div className="sprellement">
          {sprell.description}
        </div>
        {
          editable &&
          <div className="addsprell" onClick={addElementCallback}>
            +
          </div>
        }
      </div>
    )
}

type ComponentProps = {
  character: Character,
  sprellType: string,
  dispatch: (action: CharacterActions) => void
}

function SpellView(props: ComponentProps): React.ReactElement {
  const { character, sprellType, dispatch } = props

  const [sprells, setSprells] = useState(character.sprells)

  const addSprell = (): void => {
    setSprells([...sprells, { name: '', magnitude: 0, description: '' }])

  }

  //! Handles list element change
  // const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   const { id, value } = event.target
  //   const splitValue = value.split('\n')
  //   setContent(value)
  // }

  // const saveChanges = () => {
  //   const splitValue = content.split('\n')
  //   setListElement(element , splitValue)
  // }

  //TODO: see if sx prop here is what was causing issues with NumericTextField

  console.log(character)
  console.log(sprells)
  return (
    <>
      {sprells && sprells.map((sprell, index) => (
        <SingleSprellView sprell={sprell} editable={true} addElementCallback={addSprell} />
      ))}

    </>

  )
}

export default SpellView