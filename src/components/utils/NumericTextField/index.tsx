
import React from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { styled } from '@mui/material/styles';

import type { Character, Info } from '../../../data'


type ComponentProps = {
    character: Character,
    setSummary: (summary: Info) => void
}

const NumericTextField = styled(TextField)<TextFieldProps>(({ theme }) => ({
    width: "8ch"
}));

export default NumericTextField