import type { CoreStats } from '../../../data'

export function modifierCalculator(stat: number | undefined): number | undefined {
    if (stat === undefined) return stat
    if (stat === 1) return -5
    if (stat <= 3) return -4
    if (stat <= 5) return -3
    if (stat <= 7) return -2
    if (stat <= 9) return -1
    if (stat <= 11) return 0
    if (stat <= 13) return 1
    if (stat <= 15) return 2
    if (stat <= 17) return 3
    if (stat <= 19) return 4
    if (stat <= 21) return 5
    if (stat <= 23) return 6
    if (stat <= 25) return 7
    if (stat <= 27) return 8
    if (stat <= 29) return 9
    if (stat === 30) return 10

    throw Error('stats out of range')
}

export function CalculateCoreStatModifiers(stats: CoreStats): CoreStats {
    const computedStats = {
        ...stats,
        str_modifier: modifierCalculator(stats.str),
        dex_modifier: modifierCalculator(stats.dex),
        con_modifier: modifierCalculator(stats.con),
        int_modifier: modifierCalculator(stats.int),
        wis_modifier: modifierCalculator(stats.wis),
        cha_modifier: modifierCalculator(stats.cha)
    }
    return computedStats
}

export default { modifierCalculator }