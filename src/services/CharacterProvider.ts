import type { Character, Spells } from "../data"

const initialSummary = {
    name: '',
    race: '',
    class: '',
    level: 1,
    exp: 0,
    alignment: '',
    party: '',
}

const initialStats = {
    str: 0,
    str_modifier: undefined,
    dex: 0,
    dex_modifier: undefined,
    con: 0,
    con_modifier: undefined,
    int: 0,
    int_modifier: undefined,
    wis: 0,
    wis_modifier: undefined,
    cha: 0,
    cha_modifier: undefined
}

const otherStats = {
    ac: 0,
    hp_max: 0,
    hp_tmp: 0,
    hp_current: 0,
    proficiencyBonus: 0,
    passivePerception: 0,
    initiative: 0,
    speed: 0,
    vision: 0
}

const proficienciesAndLanguages = ['']

const inventory = ['']
const weapons = ['']

const featuresAndTraits = ['']
const attacks = ['']
const spells = ['']

const sprells: Spells = []
const cantrips = ['']

function CharacterProvider() {
    let character: Character = {
        isValid: false,
        summary: initialSummary,
        coreStats: initialStats,
        otherStats,
        proficiencies: proficienciesAndLanguages,
        inventory,
        weapons,
        features: featuresAndTraits,
        attacks: attacks,
        spells: spells,
        sprells: sprells,
        cantrips: cantrips 
    }

    const get = () => {
        if (character.isValid === false) {
            const savedCharacter = JSON.parse(localStorage.getItem('character') || "{}")
            if (savedCharacter?.isValid === true) {
                character = savedCharacter
            }
        }
        return character
    }

    const set = (_character: Character) => {
        character = { ..._character }
    }

    const save = () => {
        localStorage.setItem('character', JSON.stringify(character))
    }

    return { get, set, save }
}

export default CharacterProvider