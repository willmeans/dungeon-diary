import { Character, Info, CoreStats, OtherStats, ProficienciesAndLanguages, Inventory, FeaturesAndTraits, AttacksAndSpells, Spells } from "../data";

export type CharacterActions = |
{ type: 'UPDATE_SUMMARY', data: Info }
| { type: 'UPDATE_CORE_STATS', data: CoreStats}
| { type: 'UPDATE_OTHER_STATS', data: OtherStats}
| { type: 'UPDATE_PROFICIENCIES', data: ProficienciesAndLanguages}
| { type: 'UPDATE_INVENTORY', data: Inventory}
| { type: 'UPDATE_WEAPONS', data: Inventory}
| { type: 'UPDATE_FEATURES_AND_TRAITS', data: FeaturesAndTraits }
| { type: 'UPDATE_ATTACKS', data: AttacksAndSpells}
| { type: 'UPDATE_SPELLS', data: AttacksAndSpells}
| { type: 'UPDATE_SPRELLS', data: Spells}
| { type: 'UPDATE_CANTRIPS', data: AttacksAndSpells}

type Action = {
    type: string,
    data?: any
}

export default function(character: Character, action: CharacterActions): Character {
    console.log(action)

    if(action.type === 'UPDATE_SUMMARY')
      {
        return { ...character, summary: action.data }
      }
    else if(action.type === 'UPDATE_CORE_STATS')
      {
        return { ...character, coreStats: action.data }
      }
    else if(action.type === 'UPDATE_OTHER_STATS')
      {
        return { ...character, otherStats: action.data }
      }
    if(action.type === 'UPDATE_PROFICIENCIES')
      {
        return { ...character, proficiencies: action.data }
      }
    else if(action.type === 'UPDATE_INVENTORY')
      {
        return { ...character, inventory: action.data }
      }
    else if(action.type === 'UPDATE_WEAPONS')
      {
        return { ...character, weapons: action.data }
      }
    else if(action.type === 'UPDATE_FEATURES_AND_TRAITS')
      {
        return { ...character, features: action.data }
      }
    else if(action.type === 'UPDATE_ATTACKS')
      {
        return { ...character, attacks: action.data }
      }
    else if(action.type === 'UPDATE_SPELLS')
      {
        return { ...character, spells: action.data }
      }
    else if(action.type === 'UPDATE_SPRELLS')
      {
        return { ...character, sprells: action.data }
      }
    else if(action.type === 'UPDATE_CANTRIPS')
      {
        return { ...character, cantrips: action.data }
      }

    return character;
}
